---
ID: 2879
post_author: "4"
post_date: 2018-11-03 02:31:26
post_date_gmt: 2018-11-03 02:31:26
post_title: Burn
post_excerpt: Burn
post_status: inherit
comment_status: open
ping_status: closed
post_password: ""
post_name: burn-3
to_ping: ""
pinged: ""
post_modified: 2018-11-03 02:31:40
post_modified_gmt: 2018-11-03 02:31:40
post_content_filtered: ""
post_parent: 1646
guid: >
  http://localhost/wordpress/wp-content/uploads/2018/09/654a5ec9-burn.png
menu_order: 0
post_type: attachment
post_mime_type: image/png
comment_count: "0"
filter: raw
---
To discard the top card of the deck prior to dealing.