---
ID: 1381
post_author: "1"
post_date: 2018-09-06 15:25:14
post_date_gmt: 2018-09-06 15:25:14
post_title: 'Texas Holdem Poker Glossary: Cash Games'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-cash-games
to_ping: ""
pinged: ""
post_modified: 2018-11-06 12:55:37
post_modified_gmt: 2018-11-06 12:55:37
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1381
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2><b>What are Cash Games?</b></h2>
Cash Games or Side Games can be defined as a regular poker game that is non-tournament related. This variation, opposed to tournaments, shouldn't be a shorthanded game.

Another difference with tournament poker games is that players can enter and leave the table as they want, as long as they meet the requirements of the table. Even so, in contrast to tournaments, the bets are made with real money at stake and not tournament chips; which only have value on the poker table. In Cash Games each chip has cash value.

This adaptation allows more economic stability (if that's possible in gambling games) as a result of its first-come-first-served basis. Tournament players can be days, weeks, even months without winning and cashing any prize; while ring game players can cash out their money just by leaving the table and their seat. It's important to mention that the payouts of Cash Games aren't subject to 1099 or W2G reporting as the tournament payouts are. They're must only be reported if the amount exceeds $10.000 USD in a day (24 hour period).