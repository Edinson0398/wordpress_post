---
ID: 1768
post_author: "1"
post_date: 2018-09-07 15:17:24
post_date_gmt: 2018-09-07 15:17:24
post_title: 'Texas Holdem Poker Glossary: Slow Play'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-slow-play
to_ping: ""
pinged: ""
post_modified: 2018-11-07 16:08:22
post_modified_gmt: 2018-11-07 16:08:22
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1768
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h1>Slowplay</h1>
By slowplaying, you disguise the strength of your hand by feigning weakness in an attempt to deceive your opponent later on in the hand. Slowplaying is profitable if you successfully <a class="definitionMatch" title="Trap definition" href="https://en.clubpoker.net/trap/definition-584" data-ipstooltip="">trap</a>your opponent. Slowplaying is dangerous if you do not have the absolute <a class="definitionMatch" title="Nuts definition" href="https://en.clubpoker.net/nuts/definition-339" data-ipstooltip="">nuts</a> because you're giving your opponents an opportunity to <a class="definitionMatch" title="Catch definition" href="https://en.clubpoker.net/catch/definition-95" data-ipstooltip="">catch</a> up in the hand.

Se hace slowplay cuando se tiene una buena jugada y no se apuesta con fuerza, con el objetivo de que otros jugadores se queden en la mano y puedan ligar una jugada menos buena que la nuestra o intenten un farol, para entonces hacerles Raise y ganar un dinero que no se hubiera obtenido de apostar con fuerza desde el inicio.

&nbsp;

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]
<p style="padding-left: 30px;">[adinserter name="Block 13"]</p>
[adinserter name="Block 14"]

[adinserter name="Block 15"]