---
ID: 1677
post_author: "1"
post_date: 2018-09-07 14:15:57
post_date_gmt: 2018-09-07 14:15:57
post_title: 'Texas Holdem Poker Glossary: Doyle Brunson'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: >
  texas-holdem-poker-glossary-doyle-brunson
to_ping: ""
pinged: ""
post_modified: 2018-11-13 21:34:36
post_modified_gmt: 2018-11-13 21:34:36
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1677
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>Doyle Brunson</h2>
<div class="sectionIntro">

It's a Holdem hand consisting of a 10-2 (Brunson won the world championship two years in a row on the final hand with these cards).

&nbsp;
<h3>Doyle Brunson playing his favorite hand 10-2 at 2003 Worl series of poker - Video -</h3>
Interesante ver cómo este par de cartas comienza a hacer protagonismo en este juego, un minuto 31 para que observemos cómo se desarrolla esta jugada con un buen Doyle Brunson en las mismas manos de Doyle Brunson. Indudable grata situación en un partido.

https://www.youtube.com/watch?v=onl9QcYz9Cs

</div>
[adinserter name="Block 1"]

&nbsp;

<img class="alignnone wp-image-3215" src="http://localhost/wordpress/wp-content/uploads/2018/09/9025822c-14720029558_f22276cf41_z-300x199.jpg" alt="Doyle Brunson" width="826" height="548" />

&nbsp;

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]