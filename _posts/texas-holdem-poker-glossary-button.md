---
ID: 1648
post_author: "1"
post_date: 2018-09-07 13:45:02
post_date_gmt: 2018-09-07 13:45:02
post_title: 'Texas Holdem Poker Glossary: Button'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-button
to_ping: ""
pinged: ""
post_modified: 2018-11-06 18:56:13
post_modified_gmt: 2018-11-06 18:56:13
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1648
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2><b>What is a Button?</b></h2>
A Button or Dealer Button, in Texas Holdem Poker, is a token used in casinos to designate the last player to take<a href="http://localhost/wordpress/texas-holdem-poker-glossary-action/"> action</a> in the first round. Since most casinos have an employee doing the dealing, it's important to know which player is in the dealer.

If you are "on the button" it means that you're in the dealer position and you are the last player to have action during the <a href="http://localhost/wordpress/texas-holdem-poker-betting-structure/">pre-flop</a>. Being on this position it also an advantageous position, since you decide the seize of the bet of the rounds you're in. It also means that the players to your immediate left are the <a href="http://localhost/wordpress/texas-holdem-poker-glossary-blinds/">blinds</a>.

The token moves clockwise and all players must have have it.
<h3 class="title style-scope ytd-video-primary-info-renderer">Tips for Playing Texas Holdem Hands : Dealer Button in Texas Holdem - video -</h3>
&nbsp;

https://www.youtube.com/watch?v=rhK3lQ-Nmxg
<h3></h3>
<h3 class="title style-scope ytd-video-primary-info-renderer">What Does "On the Button" Mean? | Poker Tutorials - video -</h3>
Un video de Nicky Numbers cae de maravilla para entender y visualizar información sobre lo que sigbifica Button, la verdad es un termino bien sencillo de comprender, pero muy interesante, porque sin esa ficha en el medio de la partida, no habría un orden designado para dar valor a cada participación de los jugadores. En el Poker el Rol del Dealer es fundamental y sin él, los juegos tal vez serían un desastre.

&nbsp;

https://www.youtube.com/watch?v=RUdSXAa8ibw

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]

&nbsp;