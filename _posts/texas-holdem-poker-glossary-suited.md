---
ID: 1780
post_author: "1"
post_date: 2018-09-07 15:19:37
post_date_gmt: 2018-09-07 15:19:37
post_title: 'Texas Holdem Poker Glossary: Suited'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-suited
to_ping: ""
pinged: ""
post_modified: 2018-11-07 15:51:10
post_modified_gmt: 2018-11-07 15:51:10
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1780
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h1>Suited</h1>
Cards comprised of the same suit that have one gap in between each card, for example, Js-9s and 7h-5h are <a class="definitionMatch" title="Suited definition" href="https://en.clubpoker.net/suited/definition-528" data-ipstooltip="">suited</a> gappers.

Dos cartas de mano del mismo palo, como <span class="negra">A♠</span><span class="negra">9♠</span> o <span class="roja">Q♦</span><span class="roja">J♦</span>. Son deseables porque tienen una mayor probabilidad de formar color.

[adinserter name="Block 1"]
<h1></h1>
[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]
<p style="padding-left: 30px;">[adinserter name="Block 13"]</p>
[adinserter name="Block 14"]

[adinserter name="Block 15"]