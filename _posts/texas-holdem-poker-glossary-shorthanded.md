---
ID: 1762
post_author: "1"
post_date: 2018-09-07 15:16:13
post_date_gmt: 2018-09-07 15:16:13
post_title: 'Texas Holdem Poker Glossary: Shorthanded'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-shorthanded
to_ping: ""
pinged: ""
post_modified: 2018-11-07 16:12:51
post_modified_gmt: 2018-11-07 16:12:51
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1762
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>SHORTHANDED</h2>
Partida en la que intervienen de 3 a 5 jugadores.

Se denomina <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Mesa-corta/">mesa corta</a> (<i>shorthanded </i>en inglés), <i>short </i>o 6max a una partida de póker en la que toman parte enre 3 y 6 jugadores. Si hay más jugadores se llamará <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Mesa-larga/">mesa larga</a> (<i>fullring </i>en inglés). Las partidas de sólo dos jugadores se llama <i>heads-up</i> (uno contra uno).
<h3 class="title style-scope ytd-video-primary-info-renderer">Starting Hand Ranges for Shorthanded No-Limit Cash Games - Video -</h3>
Videos like this, we always like a lot, it's a good way to start with illustrations and a good sound, it definitely leads us to think that the video will be interesting; and we are not wrong; here is a man who has a very good voice, and his tone is great, then it's great the way he explain us who to make a Shorthanded. It's not a boting video, this men help us keep watching until the end. Don't stop seeing it, we assure you that you will be able to see how a game of this type unfolds.

https://www.youtube.com/watch?v=q8HzH_2Ev-Q

[adinserter name="Block 1"]

&nbsp;
<h3>Starting hand ranges for shorthanded no-limit cash games - video -</h3>
NO SÉ QUE TANTO PUEDE CALAR ESTE VIDEO... en el inicio habla sobre el tema, pero luego expone otras tempaticas y conceptos que me confunden

&nbsp;

https://www.youtube.com/watch?v=hDUt1Hq_XtQ&amp;t=186s

[adinserter name="Block 2"]
<h3></h3>
<h3>Poker VT Playing Short Handed - Video -</h3>
We like allways to see videos about the best pokers players, becasuse they are the best people to know how to play poker, Daniel Negreanu it's a good poker player, always we can to see his in diferents videos, he always explain very very well the strategys in poker. In this video you goint to see a explication about the Short Handed, it's easy to understand, and it's a good form to practice and apply this strategy.

https://www.youtube.com/watch?v=CM61WbQvciA

&nbsp;

&nbsp;

<img class="alignnone wp-image-2259" src="http://localhost/wordpress/wp-content/uploads/2018/09/6c9d7bea-poker_short_cafrino-300x179.jpg" alt="" width="721" height="430" />

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]
[adinserter name="Block 13"]
[adinserter name="Block 14"]

[adinserter name="Block 15"]