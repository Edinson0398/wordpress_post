---
ID: 1342
post_author: "1"
post_date: 2018-09-28 15:49:58
post_date_gmt: 2018-09-28 15:49:58
post_title: 'Texas Holdem Poker Glossary: Buy In'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-buy-in
to_ping: ""
pinged: ""
post_modified: 2018-11-26 18:26:14
post_modified_gmt: 2018-11-26 18:26:14
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1342
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is Buy In?</h2>
<strong>Buy in</strong>, in Texas Holdem Poker, is the amount of money a player has to pay in order to sit in a <strong>particular table</strong> or to enter a <strong>tournament game</strong>.

When it comes to <strong>particular tables</strong>, this payment represents the amount of chips a player will have during his/her table time. This since most tables have a minimum requirement or maximum amount, sometimes both.

As for <strong>tournament games</strong>, this up-front payment is made only to determine the final winning prize pool. It also encloses a fee (usually 2.5% to 10%) paid to the gambling house.

For example, in a tournament with a capacity for 100 people, the buy in could cost 110$ per player. This would be represented as 100$+10$. Meaning 100$ goes to the pool and 10$ to the house.

&nbsp;

&nbsp;

[adinserter name="Block 1"]

&nbsp;

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

&nbsp;